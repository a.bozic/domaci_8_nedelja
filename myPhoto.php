<?php
 include 'function.php';

// define variables and set to empty values
$name = $lastname = $publish = "";
$nameErr = $lastnameErr = $publishErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "This field is required!";
  }
  else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed!";
    }
    else {
      $name1 = $_POST['name'];
      // check if name contain at least four characters
      if (strlen($name1) <4) {
        $nameErr = "Name must contain at least four characters";
      }
    }
  }

  if (empty($_POST["lastname"])) {
    $lastnameErr = "This field is required";
  }
  else {
    $lastname = test_input($_POST["lastname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
      $lastnameErr = "Only letters and white space allowed!";
    }
    else {
      $lastname1 = $_POST['lastname'];
      // check if name contain at least four characters
      if (strlen($lastname1) <4) {
        $lastnameErr = "Name must contain at least four characters";
      }
    }
  }

  if (empty($_POST["publish"])) {
    $publishErr = "Choose one option!";
  } else {
    $publish = test_input($_POST["publish"]);
  }

  if (!empty($nameErr) or !empty($lastnameErr) or !empty($publishErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&lastname=" . urlencode($_POST["lastname"]);
    $params .= "&publish=" . urlencode($_POST["publish"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lastnameErr=" . urlencode($lastnameErr);
    $params .= "&publishErr=" . urlencode($publishErr);

    header("Location: index.php?" . $params);
  } else {
    echo "<a href=\"index.php\">Return to form</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
var_dump($_FILES);
if (checkImage($_FILES["photo"])) {

  //var_dump($_FILES);
  if(isset($_FILES["photo"]) AND is_uploaded_file($_FILES['photo']['tmp_name'])) {
      $file_name = $_FILES['photo']["name"];
      $file_temp = $_FILES["photo"]["tmp_name"];
      $file_size = $_FILES["photo"]["size"];
      $file_type = $_FILES["photo"]["type"];
      $file_error = $_FILES['photo']["error"];
      if ($file_error >0) {
          echo "Something went wrong!";
      }
      else {
          echo exif_imagetype($file_temp)."<br />";
          if (!exif_imagetype($file_temp)) {
            exit("File is not a picture!");
          }
          elseif (exif_imagetype($file_temp) != IMAGETYPE_JPEG && exif_imagetype($file_temp) != IMAGETYPE_PNG ){
            exit("File must be jpeg/png");
         }



          $ext_temp = explode(".", $file_name);
          $extension = end($ext_temp);



          $file_name_name = strtolower ($_POST['name'])."_".strtolower ($_POST['lastname']);
          $new_file_name = str_replace (" ", "_", $file_name_name). "-" .date("YmdHis") . "-".rand(1, 10). ".$extension";

          $directory = $_POST['publish'];
          $upload = "$directory/$new_file_name";

          // upload
          if (!is_dir($directory)) {
            mkdir($directory);
          }
          if (!file_exists($upload)) {
              if (move_uploaded_file($file_temp, $upload)) {
                  $size = getimagesize($upload);

                  foreach ($size as $key => $value)
                  echo "$key = $value<br />";
                  echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";
              }
              else {
                echo "<p><b>Error!</b></p>";
              }
          }
          else {
              for($i=1; $i<9; $i++){
                  if(!file_exists($directory.'/'.$file_name_name.$i.'.'.$extension)){
                    move_uploaded_file($file_temp, $directory.'/'.$file_name_name.$i.'.'.$extension);
                    break;
                  }
                  else{
                    echo "<p><b>File with this name already exists!</b></p>";
                  }
              }
          }
      }
  }
}
?>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Complete form</title>
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
</head>
<body>
<?php

    $name = $lastname = $publish = "";
    $nameErr = $lastnameErr = $publishErr = "";

    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['lastname'])) { $lastname = $_GET['lastname']; }
    if (isset($_GET['publish'])) { $publish = $_GET['publish']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['lastnameErr'])) { $lastnameErr = $_GET['lastnameErr']; }
    if (isset($_GET['publishErr'])) { $publishErr = $_GET['publishErr']; }
?>
    <h3>Complete form!</h3>

    <form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >

        <input type="text" name="name" value="<?php echo $name;?>" placeholder="Name">
        <span class="error">* <?php echo $nameErr;?></span>
        <br><br>

        <input type="text" name="lastname" value="<?php echo $lastname;?>" placeholder="Lastname">
        <span class="error">* <?php echo $lastnameErr;?></span>
        <br><br>

        <p>Directory for picture:</p>
        <input class="publish" type="radio" name="publish" <?php if (isset($publish) && $publish=="public") echo "checked";?> value="public">Public
        <input class="publish" type="radio" name="publish" <?php if (isset($publish) && $publish=="private") echo "checked";?> value="private">Private
        <span class="error">* <?php echo $publishErr;?></span>
        <br>

        <label for="photo">File:</label>
        <input id="file" type="file" name="photo" id="photo"/><br /><br />
        <input id="submit" type="submit" name="sb" id="sb" value="upload" />
        <input id="reset" type="reset" name="rb" id="rb" value="cancel" />
    </form>
</body>
</html>
